package ru.mirsaitov.chat.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashUtil {

    /**
     * Hash message by MD5
     *
     * @param message
     * @return hash
     */
    public static String hashMD5(final String message) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(message.getBytes());
            byte[] digest = md.digest();
            return new String(digest);
        } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
            return null;
        }
    }

}
