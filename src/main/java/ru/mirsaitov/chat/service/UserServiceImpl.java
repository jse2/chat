package ru.mirsaitov.chat.service;

import org.springframework.stereotype.Service;
import ru.mirsaitov.chat.domain.User;
import ru.mirsaitov.chat.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public Optional<User> findByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    @Override
    @Transactional
    public User save(User user) {
        return userRepository.save(user);
    }

}
