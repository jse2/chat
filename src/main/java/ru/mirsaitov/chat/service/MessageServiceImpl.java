package ru.mirsaitov.chat.service;

import org.springframework.stereotype.Service;
import ru.mirsaitov.chat.domain.Message;
import ru.mirsaitov.chat.repository.MessageRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class MessageServiceImpl implements MessageService {

    private final MessageRepository messageRepository;

    public MessageServiceImpl(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @Override
    @Transactional
    public Message save(Message message) {
        return messageRepository.save(message);
    }

    @Override
    @Transactional
    public List<Message> findByChatIdAndIdGreaterThan(Long chatId, Long id) {
        return messageRepository.findByChatIdAndIdGreaterThan(chatId, id);
    }

}
