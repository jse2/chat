package ru.mirsaitov.chat.service;

import ru.mirsaitov.chat.domain.Chat;

import java.util.Optional;

public interface ChatService {

    Optional<Chat> findByName(String name);

    Chat save(Chat chat);

}
