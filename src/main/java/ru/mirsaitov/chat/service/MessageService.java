package ru.mirsaitov.chat.service;

import ru.mirsaitov.chat.domain.Message;

import java.util.List;

public interface MessageService {

    Message save(Message message);

    List<Message> findByChatIdAndIdGreaterThan(Long chatId, Long id);

}
