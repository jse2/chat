package ru.mirsaitov.chat.service;

import ru.mirsaitov.chat.domain.User;

import java.util.Optional;

public interface UserService {

    Optional<User> findByLogin(String login);

    User save(User user);

}
