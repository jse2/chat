package ru.mirsaitov.chat.service;

import org.springframework.stereotype.Service;
import ru.mirsaitov.chat.domain.Chat;
import ru.mirsaitov.chat.repository.ChatRepository;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class ChatServiceImpl implements ChatService {

    private final ChatRepository chatRepository;

    public ChatServiceImpl(ChatRepository chatRepository) {
        this.chatRepository = chatRepository;
    }

    @Override
    @Transactional
    public Optional<Chat> findByName(String name) {
        return chatRepository.findByName(name);
    }

    @Override
    @Transactional
    public Chat save(Chat chat) {
        return chatRepository.save(chat);
    }

}
