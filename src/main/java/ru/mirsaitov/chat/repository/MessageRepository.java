package ru.mirsaitov.chat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.mirsaitov.chat.domain.Message;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {

    List<Message> findByChatIdAndIdGreaterThan(Long chatId, Long id);

}
