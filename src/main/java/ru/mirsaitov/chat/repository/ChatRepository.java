package ru.mirsaitov.chat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.mirsaitov.chat.domain.Chat;

import java.util.Optional;

public interface ChatRepository extends JpaRepository<Chat, Long> {

    Optional<Chat> findByName(String name);

}
