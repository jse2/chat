package ru.mirsaitov.chat;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.Connection;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import ru.mirsaitov.chat.domain.Chat;
import ru.mirsaitov.chat.domain.Message;
import ru.mirsaitov.chat.domain.User;
import ru.mirsaitov.chat.rabbitmq.MessageReceiver;
import ru.mirsaitov.chat.service.ChatService;
import ru.mirsaitov.chat.service.MessageService;
import ru.mirsaitov.chat.service.UserService;
import ru.mirsaitov.chat.util.HashUtil;

import java.io.IOException;
import java.util.Optional;
import java.util.Scanner;

@Component
public class App implements CommandLineRunner {

    private final Scanner scanner = new Scanner(System.in, "UTF-8");

    private final ChatService chatService;

    private final UserService userService;

    private final MessageService messageService;

    private final CachingConnectionFactory connectionFactory;

    private final RabbitTemplate template;

    private FanoutExchange exchange;

    private SimpleMessageListenerContainer container;

    @Autowired
    public App(ChatService chatService, UserService userService, MessageService messageService, CachingConnectionFactory connectionFactory, RabbitTemplate template) {
        this.chatService = chatService;
        this.userService = userService;
        this.messageService = messageService;
        this.connectionFactory = connectionFactory;
        this.template = template;
    }

    private void loop(User user, Chat chat) {
        String msg;
        while (scanner.hasNextLine()) {
            msg = scanner.nextLine();
            if ("exit".equals(msg)) {
                break;
            } else if (!msg.isBlank()) {
                messageService.save(new Message()
                        .setUserId(user.getId())
                        .setChatId(chat.getId())
                        .setText(msg)
                );
                template.convertAndSend(exchange.getName(), "", user.getLogin() + ": " + msg);
            }
        }
        container.stop();
        connectionFactory.resetConnection();
    }

    private Chat selectChat() {
        System.out.println("Please enter name chat:");
        String name = scanner.nextLine();
        return chatService.findByName(name).orElseGet(() -> createChat(name));
    }

    private Chat createChat(String name) {
        Chat chat = new Chat().setName(name);
        return chatService.save(chat);
    }

    private User login() {
        Optional<User> user = Optional.empty();
        boolean isLogin = false;
        while (!isLogin) {
            System.out.println("Please enter login:");
            String login = scanner.nextLine();
            user = userService.findByLogin(login);
            user = user.isPresent() ? user.map(this::checkPassword) : createUser(login);
            isLogin = user.isPresent();
        }
        return user.get();
    }

    private Optional<User> createUser(String login) {
        System.out.println("Please enter password:");
        String password = HashUtil.hashMD5(scanner.nextLine());
        User user = new User().setLogin(login)
                .setPassword(password);
        return Optional.of(userService.save(user));
    }

    private User checkPassword(User user) {
        System.out.println("Please enter password:");
        String password = HashUtil.hashMD5(scanner.nextLine());
        boolean isCheck = password.equals(user.getPassword());
        if (!isCheck) {
            System.out.println("Password not correct");
        }
        return isCheck ? user : null;
    }

    @Override
    public void run(String... args) throws Exception {
        User user = login();
        Chat chat = null;
        while (chat == null || chat.getId() == null) {
            chat = selectChat();
            setupRabbit(chat.getName());
        }
        System.out.println("You are entered in chat " + chat.getName());
        loop(user, chat);
    }

    private void setupRabbit(String chatName) {
        exchange = new FanoutExchange(chatName);
        try (Connection connection = connectionFactory.createConnection();
             Channel channel = connection.createChannel(false)
        ) {
            ensureExchangeExist(channel, exchange.getName());
            String queueName = channel.queueDeclare().getQueue();
            channel.queueBind(queueName, exchange.getName(), "");
            buildListener(queueName);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private void ensureExchangeExist(Channel channel, String exchangeName) throws IOException {
        try {
            channel.exchangeDeclarePassive(exchangeName);
        } catch (Exception e) {
            channel.exchangeDeclare(exchangeName, BuiltinExchangeType.FANOUT);
        }
    }

    private void buildListener(String queueName) {
        MessageListenerAdapter adapter = new MessageListenerAdapter(new MessageReceiver(), "receiveMessage");
        container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(queueName);
        container.setMessageListener(adapter);
        container.initialize();
        container.start();
    }

}
